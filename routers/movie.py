from fastapi import APIRouter
from fastapi import Path, Query, Depends
from fastapi.responses import JSONResponse
from pydantic import BaseModel,Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie_service import MovieService
from schemas.movie import Movie


movie_router = APIRouter()


@movie_router.get('/movies',tags=['Movies'], response_model=list[Movie])
def get_movies()-> List [Movie]:
        db = Session()
        result = MovieService(db).get_movies()
        return JSONResponse(content= jsonable_encoder(result))

@movie_router.get('/movies/{id}',tags=['Movies'], response_model=Movie)
def get_movie(id:int= Path(ge=1,le=2000))-> Movie:
        db = Session()
        result = MovieService(db).get_movie(id)
        if not result:
               return JSONResponse(status_code=404, content={'message': "no found"})
        return JSONResponse(content= jsonable_encoder(result))
        

@movie_router.get('/movies/',tags=['Movies'],response_model=list[Movie])
def get_movies_by_category(category:str =Query(min_length=5,max_length=15))-> Movie:
        db = Session()
        result = MovieService(db).get_movie_cat(category)
        if not result:
               return JSONResponse(status_code=404, content={'message': "no found"})
        return JSONResponse(content= jsonable_encoder(result))


@movie_router.post('/movies',tags=['Movies'],response_model=dict)
def create_movie(movie:Movie)-> dict:
        db = Session()
        MovieService(db).create_movie(movie)
        return JSONResponse(content={"message":"Se ha agregado la pelicula"})

@movie_router.put('/movies/{id}', tags=['Movies'],response_model=dict)
def update_movie(id:int,movie:Movie)-> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
       return JSONResponse(status_code=404, content={'message': "no found"})
    MovieService(db).update_movie(id,movie)
    return JSONResponse(content={"message": "Update"})

@movie_router.delete('/movies/{id}', tags=['Movies'],response_model=dict)
def delete_movie(id:int)-> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
       return JSONResponse(status_code=404, content={'message': "no found"})
    MovieService(db).delete_movie(id)
    return JSONResponse(content={"message":"Se elimino la pelicula"})
